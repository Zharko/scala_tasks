import Deps._

name := "task2-ieither"

version := "0.1"

scalaVersion := "2.13.4"

ThisBuild / organization := "ru.dins.scala-school"

libraryDependencies ++= Seq(
  scalatest,
  scalacheck
)

lazy val printTestClasspath = taskKey[Unit]("Dump Test classpath")

printTestClasspath := {
  val classPath = (fullClasspath in Test value)
  val string = classPath.map(_.data.getCanonicalPath).mkString(":")
  new java.io.PrintWriter("classpath.txt") { write(string); close() }
}
