package ru.dins.scalaschool.ieither

import jdk.jshell.spi.ExecutionControl.InternalException
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

class IEitherTest extends AnyFlatSpec {

  val boolTest: Boolean = true
  val intTest: Integer  = 1
  val strTest: String   = "string"

  def throwException(value: Boolean): String                     = throw new Exception
  def throwYaException(value: Boolean, yaValue: Boolean): String = throw new Exception

  "IEither.map" should "not call passed function if only left value is present" in {
    IEither.left[Int, Boolean](intTest).map(throwException) shouldBe IEither.left[Int, String](intTest)
  }

  it should "change right value according to passed function if only right value is present" in {
    IEither.right(intTest).map(_ => strTest) shouldBe IEither.right(strTest)
  }

  it should "not change left value and change only right according to passed function if both value are present" in {
    IEither.both(intTest, boolTest).map(_ => strTest) shouldBe IEither.both(intTest, strTest)
  }

  "IEither.leftMap" should "not call passed function if only right value is present" in {
    IEither.right[Boolean, Int](intTest).leftMap(throwException) shouldBe IEither.right[String, Int](intTest)
  }

  it should "change left value according to passed function if only left value is present" in {
    IEither.left(intTest).leftMap(_ => strTest) shouldBe IEither.left(strTest)
  }

  it should "not change right value and change only left according to passed function if both value are present" in {
    IEither.both(intTest, boolTest).leftMap(_ => strTest) shouldBe IEither.both(strTest, boolTest)
  }

  "IEither.bimap" should "change left and right values according to passed function" in {
    IEither.both(intTest, boolTest).bimap(_ => strTest, _ => intTest) shouldBe IEither.both(strTest, intTest)
  }

  it should "not call passed function for right value and change only left value if only left value is present" in {
    IEither.left[Int, Boolean](intTest).bimap(_ => strTest, throwException) shouldBe IEither.left[String, String](
      strTest,
    )
  }

  it should "not call passed function for left value and change only right value if only right value is present" in {
    IEither.right[Boolean, Int](intTest).bimap(throwException, _ => strTest) shouldBe IEither.right[String, String](
      strTest,
    )
  }

  "IEither.flatMap" should "not change IEither if right isn't present" in {
    IEither.left[Int, String](intTest).flatMap(_ => IEither.right(boolTest)) shouldBe IEither.left(intTest)
  }

  it should "return result of f applied to right if left isn't present" in {
    IEither.right(intTest).flatMap(_ => IEither.left(boolTest)) shouldBe IEither.left(boolTest)
  }

  it should "preserve left value if f applied to right doesn't contain left value" in {
    IEither.both(intTest, boolTest).flatMap(_ => IEither.right(strTest)) shouldBe IEither.both(intTest, strTest)
  }

  it should "drop left value if f applied to right contains left value" in {
    IEither.both(intTest, boolTest).flatMap(_ => IEither.both(intTest, strTest)) shouldBe IEither.both(intTest, strTest)
  }

  "IEither.fold" should "call fab function if both values are present" in {
    IEither.both(boolTest, boolTest).fold(throwException, throwException, (_, _) => strTest) shouldBe strTest
  }

  it should "call fa function if only left value is present" in {
    IEither.left[Boolean, Boolean](boolTest).fold(_ => strTest, throwException, throwYaException) shouldBe strTest
  }

  it should "call fb function if only right value is present" in {
    IEither.right[Boolean, Boolean](boolTest).fold(throwException, _ => strTest, throwYaException) shouldBe strTest
  }

  "IEither.swap" should "change left value to right value if only left value is present" in {
    IEither.left[Int, Boolean](intTest).swap shouldBe IEither.right[Boolean, Int](intTest)
  }

  it should "change right value to left value if only right value is present" in {
    IEither.right[Int, Boolean](boolTest).swap shouldBe IEither.left[Boolean, Int](boolTest)
  }

  it should "change left value to right value and vice versa if both values are present" in {
    IEither.both(intTest, boolTest).swap shouldBe IEither.both(boolTest, intTest)
  }

  "IEither.left" should "return left value if only left value is present" in {
    IEither.left(intTest).left shouldBe Some(intTest)
  }

  it should "return left value if both values are present" in {
    IEither.both(intTest, boolTest).left shouldBe Some(intTest)
  }

  it should "return none if only right value is present" in {
    IEither.right(boolTest).left shouldBe None
  }

  "IEither.right" should "return right value if only right value is present" in {
    IEither.right(intTest).right shouldBe Some(intTest)
  }

  it should "return right value if both values are present" in {
    IEither.both(intTest, boolTest).right shouldBe Some(boolTest)
  }

  it should "return none if only left value is present" in {
    IEither.left(intTest).right shouldBe None
  }

  "IEither.toEither" should "convert left value to Either if only left value is present" in {
    IEither.left(intTest).toEither shouldBe scala.util.Left(intTest)
  }

  it should "convert right value to Either if only right value is present" in {
    IEither.right(intTest).toEither shouldBe scala.util.Right(intTest)
  }

  it should "convert right value to Either if both values are present" in {
    IEither.both(intTest, boolTest).toEither shouldBe scala.util.Right(boolTest)
  }

}
