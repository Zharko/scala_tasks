package ru.dins.scalaschool.ieither

/** Может содержать либо значение типа A, либо значение типа B, либо по одному значению типов A и B одновременно.
  * Не может быть пустым. Right-biased, то есть значение правой части считается основным.
  * @tparam A Тип левой части
  * @tparam B Тип правой части
  */
sealed trait IEither[+A, +B] {

  /** Изменяет правую часть с помощью заданной функции.
    * @param f Функция, которая будет применена к правой части
    * @tparam C Новый тип правой части
    * @return IEither, у которого левая часть без изменений, а правая заменена на результат применения f к правой части,
    *         если она существует
    */
  def map[C](f: B => C): IEither[A, C] = this match {
    case IRight(b)   => IRight(f(b))
    case IBoth(a, b) => IBoth(a, f(b))
    case _           => this.asInstanceOf[IEither[A, C]]
  }

  /** Изменяет левую часть с помощью заданной функции.
    * @param f Функция, которая будет применена к левой части
    * @tparam C Новый тип левой части
    * @return IEither, у которого правая часть без изменений, а левая заменена на результат применения f к левой части,
    *         если она существует
    */
  def leftMap[C](f: A => C): IEither[C, B] = this match {
    case ILeft(a)    => ILeft(f(a))
    case IBoth(a, b) => IBoth(f(a), b)
    case _           => this.asInstanceOf[IEither[C, B]]
  }

  /** Изменяет обе части с помощью заданной функции.
    * @param fa Функция, которая будет применена к левой части
    * @param fb Функция, которая будет применена к правой части
    * @tparam C Новый тип левой части
    * @tparam D Новый тип правой части
    * @return IEither, у которого левая часть заменена на результат применения f к левой части, а правая заменена на
    *         результат применения f к правой части. Если какая-то из частей отсутствует, то в возвращенном IEither
    *         она тоже отсутствует.
    */
  def bimap[C, D](fa: A => C, fb: B => D): IEither[C, D] = this match {
    case IBoth(a, b) => IBoth(fa(a), fb(b))
    case ILeft(a)    => ILeft(fa(a))
    case IRight(b)   => IRight(fb(b))
  }

  /** Применяет функцию к правой части, а полученный результат объединяет с изначальным IEither.
    * @param f Функция, которая будет применена к правой части
    * @tparam C Новый тип левой части и тип левой части значения функции f
    * @tparam D Новый тип правой части и тип правой части значения функции f
    * @return Результат применения функции f, у которого правая часть остается без изменений, а левая часть выбирается
    *         следующим образом:
    *
    *         1) Если левая часть есть в результате применения функции f, то левой частью должна быть именно она.
    *
    *         2) Иначе, если левая часть есть только в изначальном IEither, то левой частью должна быть она.
    *
    *         3) Иначе, если левой части не было изначально и нет в результате применения f, то левой части быть не должно.
    *
    *         Пример:
    *         {{{
    *           IEither.left[Int, String](4).flatMap(_ => IEither.right(true)) == IEither.left(4)
    *           IEither.right(5).flatMap(_ => IEither.left(true))              == IEither.left(true)
    *           IEither.both(6, true).flatMap(_ => IEither.right("string"))    == IEither.both(6, "string")
    *           IEither.both(6, true).flatMap(_ => IEither.both(1, "string"))  == IEither.both(1, "string")
    *         }}}
    */
  def flatMap[C >: A, D](f: B => IEither[C, D]): IEither[C, D]

  /** Применяет одну из трех функций к значению/значениям внутри и возвращает результат. Остальные функции не должны быть
    * вызваны.
    * @param fa Функция, которая будет применена, если есть только левая часть
    * @param fb Функция, которая будет применена, если есть только правая часть
    * @param fab Функция, которая будет применена, если есть обе части
    * @return Результат применения одной из трех функций
    */
  def fold[C](fa: A => C, fb: B => C, fab: (A, B) => C): C = this match {
    case ILeft(a)    => fa(a)
    case IRight(b)   => fb(b)
    case IBoth(a, b) => fab(a, b)
  }

  /** Меняет местами левую и правую части.
    */
  def swap: IEither[B, A] = this match {
    case ILeft(a)    => IRight(a)
    case IRight(b)   => ILeft(b)
    case IBoth(a, b) => IBoth(b, a)
  }

  /** Получение левой части
    * @return Если левой части нет, возвращает None. Иначе - Some со значением левой части.
    */
  def left: Option[A] = this match {
    case ILeft(a)    => Some(a)
    case IBoth(a, _) => Some(a)
    case _           => None
  }

  /** Получение правой части
    * @return Если правой части нет, возвращает None. Иначе - Some со значением правой части.
    */
  def right: Option[B] = this match {
    case IRight(b)   => Some(b)
    case IBoth(_, b) => Some(b)
    case _           => None
  }

  /** Преобразует в Either.
    * @return Если есть только левая часть, то возвращает [[scala.util.Left]] со значением левой части.
    *         Иначе - [[scala.util.Right]] со значением правой части. Если есть обе, то левая отбрасывается.
    */
  def toEither: Either[A, B] = this match {
    case ILeft(a)    => scala.util.Left(a)
    case IRight(b)   => scala.util.Right(b)
    case IBoth(_, b) => scala.util.Right(b)
  }
}

final case class ILeft[+A, +B](value: A) extends IEither[A, B] {
  override def flatMap[C >: A, D](f: B => IEither[C, D]): IEither[C, D] = this.asInstanceOf[IEither[C, D]]
}

final case class IRight[+A, +B](value: B) extends IEither[A, B] {
  override def flatMap[C >: A, D](f: B => IEither[C, D]): IEither[C, D] = f(value)
}

final case class IBoth[+A, +B](lValue: A, rValue: B) extends IEither[A, B] {
  override def flatMap[C >: A, D](f: B => IEither[C, D]): IEither[C, D] = f(rValue) match {
    case IRight(rValue) => IBoth(lValue, rValue)
    case other          => other
  }
}

object IEither {

  /** Создает IEither из значения левой части
    * @return IEither, который содержит `a` в качестве левой части и не содержит правой части
    */
  def left[A, B](a: A): IEither[A, B] = ILeft(a)

  /** Создает IEither из значения правой части
    * @return IEither, который содержит `b` в качестве правой части и не содержит левой части
    */
  def right[A, B](b: B): IEither[A, B] = IRight(b)

  /** Создает IEither из значений правой и левой частей
    * @return IEither, который содержит `a` в качестве левой части, а `b` в качестве правой.
    */
  def both[A, B](a: A, b: B): IEither[A, B] = IBoth(a, b)
}
