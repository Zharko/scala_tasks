import sbt._

object Deps {
  val scalatest = "org.scalatest" %% "scalatest" % "3.2.2" % Test
  val scalacheck = "org.scalacheck" %% "scalacheck" % "1.15.2" % Test
}
