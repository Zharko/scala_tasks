import Deps._

name := "task1-scala-life"

version := "0.1"

scalaVersion := "2.13.4"

ThisBuild / organization := "ru.dins.scala-school"

libraryDependencies ++= scalaFX ++ Seq(
  scalacheck,
  scalatest,
)

lazy val printTestClasspath = taskKey[Unit]("Dump Test classpath")

printTestClasspath := {
  val classPath = (fullClasspath in Test value)
  val string = classPath.map(_.data.getCanonicalPath).mkString(":")
  new java.io.PrintWriter("classpath.txt") { write(string); close() }
}