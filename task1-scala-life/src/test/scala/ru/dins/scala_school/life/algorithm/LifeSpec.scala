package ru.dins.scala_school.life.algorithm

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class LifeSpec extends AnyFunSpec with Matchers {

  private val `X`: Boolean  = true
  private val `_` : Boolean = false

  describe("Any live cell with fewer than two live neighbours dies, as if by underpopulation.") {

    it("without neighbour should die") {
      val seed = Vector(
        Vector(`_`, `_`, `_`),
        Vector(`_`, `X`, `_`),
        Vector(`_`, `_`, `_`),
      )
      Life.tick(seed) shouldBe empty(3)
    }

    it("with one neighbour should die") {
      val seed = Vector(
        Vector(`_`, `_`, `_`, `_`),
        Vector(`_`, `X`, `X`, `_`),
        Vector(`_`, `_`, `_`, `_`),
        Vector(`_`, `_`, `_`, `_`),
      )
      Life.tick(seed) shouldBe empty(4)
    }
  }

  describe("Any live cell with two or three live neighbours lives on to the next generation.") {

    it("with two neighbours should survive") {
      val seed = Vector(
        Vector(`_`, `_`, `X`, `_`),
        Vector(`_`, `X`, `_`, `_`),
        Vector(`X`, `_`, `_`, `_`),
        Vector(`_`, `_`, `_`, `_`),
      )
      Life.tick(seed) shouldBe Vector(
        Vector(`_`, `_`, `_`, `_`),
        Vector(`_`, `X`, `_`, `_`),
        Vector(`_`, `_`, `_`, `_`),
        Vector(`_`, `_`, `_`, `_`),
      )
    }

    it("with three neighbours should survive") {
      val seed = Vector(
        Vector(`_`, `_`, `_`, `_`),
        Vector(`_`, `X`, `X`, `_`),
        Vector(`_`, `X`, `X`, `_`),
        Vector(`_`, `_`, `_`, `_`),
      )
      Life.tick(seed) shouldBe Vector(
        Vector(`_`, `_`, `_`, `_`),
        Vector(`_`, `X`, `X`, `_`),
        Vector(`_`, `X`, `X`, `_`),
        Vector(`_`, `_`, `_`, `_`),
      )
    }
  }

  it("Any live cell with more than three live neighbours dies, as if by overpopulation.") {
    val seed = Vector(
      Vector(`_`, `_`, `_`, `_`, `_`, `_`),
      Vector(`_`, `X`, `X`, `_`, `_`, `_`),
      Vector(`_`, `X`, `X`, `_`, `_`, `_`),
      Vector(`_`, `_`, `_`, `X`, `X`, `_`),
      Vector(`_`, `_`, `_`, `X`, `X`, `_`),
      Vector(`_`, `_`, `_`, `_`, `_`, `_`),
    )
    Life.tick(seed) shouldBe Vector(
      Vector(`_`, `_`, `_`, `_`, `_`, `_`),
      Vector(`_`, `X`, `X`, `_`, `_`, `_`),
      Vector(`_`, `X`, `_`, `_`, `_`, `_`),
      Vector(`_`, `_`, `_`, `_`, `X`, `_`),
      Vector(`_`, `_`, `_`, `X`, `X`, `_`),
      Vector(`_`, `_`, `_`, `_`, `_`, `_`),
    )
  }

  it("Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.") {
    val seed = Vector(
      Vector(`_`, `_`, `_`, `_`),
      Vector(`_`, `X`, `X`, `_`),
      Vector(`_`, `X`, `_`, `_`),
      Vector(`_`, `_`, `_`, `_`),
    )
    Life.tick(seed) shouldBe Vector(
      Vector(`_`, `_`, `_`, `_`),
      Vector(`_`, `X`, `X`, `_`),
      Vector(`_`, `X`, `X`, `_`),
      Vector(`_`, `_`, `_`, `_`),
    )
  }

  it("Life happens on cyclic field") {
    val seed = Vector(
      Vector(`_`, `_`, `_`, `_`, `X`, `_`),
      Vector(`_`, `_`, `_`, `_`, `X`, `X`),
      Vector(`_`, `_`, `_`, `_`, `_`, `_`),
      Vector(`_`, `_`, `_`, `_`, `_`, `_`),
      Vector(`X`, `X`, `_`, `_`, `_`, `_`),
      Vector(`_`, `X`, `_`, `_`, `_`, `_`),
    )
    Life.tick(seed) shouldBe Vector(
      Vector(`_`, `_`, `_`, `_`, `X`, `X`),
      Vector(`_`, `_`, `_`, `_`, `X`, `X`),
      Vector(`_`, `_`, `_`, `_`, `_`, `_`),
      Vector(`_`, `_`, `_`, `_`, `_`, `_`),
      Vector(`X`, `X`, `_`, `_`, `_`, `_`),
      Vector(`X`, `X`, `_`, `_`, `_`, `_`),
    )
  }

  private def empty(n: Int): Vector[Vector[Boolean]] = Vector.fill(n)(Vector.fill(n)(`_`))

}
