package ru.dins.scala_school.life.algorithm

import scala.annotation.tailrec
import scala.language.implicitConversions

object Life {
  implicit def bool2int(b: Boolean): Int = if (b) 1 else 0

  def neighborCount(elements: Vector[Vector[Boolean]], row: Int, column: Int): Int = {
    val aliveNeighbour = for {
      i <- -1 to 1
      j <- -1 to 1
      if i != 0 || j != 0
    } yield elements(row + i)(column + j)
    val count = aliveNeighbour.count(_ == true)
    count
  }

  def lifeCondition(cellState: Boolean, num: Int): Boolean =
    (cellState, num) match {
      case (_, 3)    => true
      case (true, 2) => true
      case _         => false
    }

  def tick(state: Vector[Vector[Boolean]]): Vector[Vector[Boolean]] = {
    val stateExtended = state.last +: state :+ state.head
    val stateImproved = stateExtended.map(x => x.last +: x :+ x.head)

    val iMax = state.size
    val jMax = state.head.size

    val tempSequence = for {
      i <- 1 to iMax
      j <- 1 to jMax
    } yield lifeCondition(stateImproved(i)(j), neighborCount(stateImproved, i, j))

    val newVector = tempSequence.toVector
    val result    = newVector.grouped(jMax).toVector
    result
  }
}
