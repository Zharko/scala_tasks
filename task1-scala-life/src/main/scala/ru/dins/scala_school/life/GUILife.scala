package ru.dins.scala_school.life

import ru.dins.scala_school.life.algorithm.Life
import scalafx.animation.{KeyFrame, Timeline}
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.canvas.Canvas
import scalafx.scene.layout.BorderPane
import scalafx.scene.paint.Color
import scalafx.scene.{Group, Scene}
import scalafx.util.Duration

import scala.util.Random

object GUILife extends JFXApp {
  val testTubeCanvas = new CellCanvas

  stage = new PrimaryStage {

    title = "Game of Life"
    resizable = false

    scene = new Scene {
      root = new BorderPane {
        center = new Group {
          testTubeCanvas.width = 800
          testTubeCanvas.height = 600
          children = testTubeCanvas
        }
      }
    }
  }

  private val timeline = new Timeline {
    cycleCount = Timeline.Indefinite
    keyFrames = Seq(
      KeyFrame(
        Duration(100),
        onFinished = _ => {
          val seed = Life.tick(testTubeCanvas.seed)
          testTubeCanvas.update(seed)
        },
      ),
    )
  }
  timeline.play()

  class CellCanvas extends Canvas {

    private val CellSize               = 10
    private val gc                     = graphicsContext2D
    private var cells: Set[(Int, Int)] = List.fill(500)(Random.nextInt(80) -> Random.nextInt(60)).toSet
    gc.fill = Color.HotPink

    def seed: Vector[Vector[Boolean]] = cells.foldLeft[Vector[Vector[Boolean]]](
      Vector.fill(width.value.toInt / CellSize)(Vector.fill(height.value.toInt / CellSize)(false)),
    ) { case (seed, (x, y)) =>
      seed.updated(x, seed.apply(x).updated(y, true))
    }

    def clear(): Unit = {
      gc.clearRect(0, 0, width.value, height.value)
      cells = Set.empty
    }

    private def plotCell(x: Int, y: Int): Unit = {
      gc.fillRect(x * CellSize, y * CellSize, CellSize, CellSize)
      cells = cells + ((x, y))
    }

    def update(seed: Vector[Vector[Boolean]]) = {
      clear()
      for {
        (line, x)  <- seed.zipWithIndex
        (value, y) <- line.zipWithIndex if value
      } testTubeCanvas.plotCell(x, y)
    }
  }

}
