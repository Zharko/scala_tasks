package ru.dins.scala_school.life

import ru.dins.scala_school.life.algorithm.Life

import scala.annotation.tailrec

object ConsoleLife extends App {

  def printBoard(board: Vector[Vector[Boolean]]): Unit = {
    board.map(_.map(cell => if (cell) "X" else "_").mkString("")).foreach(println)
    println()
  }

  @tailrec
  def runGame(tickLimit: Int, initialState: Vector[Vector[Boolean]]): Vector[Vector[Boolean]] = {
    printBoard(initialState)
    if (tickLimit > 0) {
      runGame(tickLimit - 1, Life.tick(initialState))
    } else
      initialState
  }

  runGame(
    10,
    Vector(
      Vector.fill(5)(false),
      Vector(false, false, true, false, false),
      Vector(false, false, false, true, false),
      Vector(false, true, true, true, false),
      Vector.fill(5)(false),
      Vector.fill(5)(false),
    ),
  )
}
