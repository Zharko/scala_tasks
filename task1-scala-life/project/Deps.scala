import sbt._

object Deps {

  val scalaFX = {

    val scalaFX = Seq("org.scalafx" %% "scalafx" % "15.0.1-R21")

    // Determine OS version of JavaFX binaries
    lazy val osName = System.getProperty("os.name") match {
      case n if n.startsWith("Linux") => "linux"
      case n if n.startsWith("Mac") => "mac"
      case n if n.startsWith("Windows") => "win"
      case _ => throw new Exception("Unknown platform!")
    }

    // Add dependency on JavaFX libraries, OS dependent
    lazy val javaFXModules = Seq("base", "controls", "fxml", "graphics", "media", "swing", "web")

    javaFXModules.map(m =>
      "org.openjfx" % s"javafx-$m" % "15.0.1" classifier osName
    ) ++ scalaFX
  }

  val scalacheck = "org.scalacheck" %% "scalacheck" % "1.15.2" % Test

  val scalatest = "org.scalatest" %% "scalatest" % "3.2.2" % Test
}
