import scala.annotation.tailrec

/** Вася очень любит сериал "Улица разбитых фонарей" по НТВ.
  * Сегодня как раз выход новой серии! Но как на зло время трансляции
  * серии пересекается с семинаром по функциональному программированию
  * в scala-школе от компании Dins.
  *
  * Вася очень ответственный молодой человек, и старается не пропускать
  * ни одного занятия. К счастью, Вася уже знает что такое функциональное
  * программирование, и что серию можно скачать из интернета!
  *
  * Вася решил написать функциональную программу с использованием IO, чтобы
  * скачать серию, пока он посещает семинар. Но к сожалению, Вася еще не
  * разобрался с монадами и не может закончить начатое :(
  *
  * Помоги Васе! Будь человеком!
  */
object Homework {

  /** Интерфейс монадки IO который надо реализовать */
  sealed trait IO[+A] {
    def map[B](f: A => B): IO[B] = flatMap(a => IO.Return(() => f(a)))

    def flatMap[B](f: A => IO[B]): IO[B] = IO.FlatMap(this, f)

    def unsafeRun: A = IO.run(this)
  }

  object IO {
    final private case class Return[A](value: () => A)                extends IO[A]
    final private case class FlatMap[A, B](sub: IO[A], f: A => IO[B]) extends IO[B]

    def apply[A](f: => A): IO[A] = Return(() => f)

    @tailrec
    private def run[A](tr: IO[A]): A = tr match {
      case Return(value) => value()
      case FlatMap(sub, f) =>
        sub match {
          case Return(value)       => run(f(value()))
          case FlatMap(yasub, yaf) => run(yasub.flatMap(yaf(_) flatMap f))
        }
    }
  }

  /** Это все, что успел написать Вася. Помоги ему закончить программу! */
  class DownloadMovies(renTvServer: RenTvClient, moviesDirectory: LocalMoviesDirectory) {
    def run(): IO[Unit] = {
      val res = renTvServer.downloadLastEpisode().flatMap(episode => moviesDirectory.save(episode))
      res
    }
  }

  /** Представляет собой очередной эпизод сериала */
  case class Episode(number: Int)

  /** Интерфейс сервиса для скачивания свежей серии, который Вася уже реализовал */
  trait RenTvClient {
    def downloadLastEpisode(): IO[Episode]
  }

  /** Интерфейс для сохранения скачанной серии. Вася уже справился с его реализацией */
  trait LocalMoviesDirectory {
    def save(episode: Episode): IO[Unit]
  }
}
