import org.scalacheck.{Arbitrary, Gen}
import org.scalactic.{Equivalence, TypeCheckedTripleEquals}
import org.scalatest.{Assertion, Assertions}
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.must.Matchers
import Homework._

class HomeworkTest extends AnyFreeSpec with Matchers {
  "Vasia's program must download new episode and put it to the directory (+2 points for Gryffindor)" in {
    // given:
    val episode = Episode(100500)
    val renTvClient = new RenTvClient {
      override def downloadLastEpisode(): IO[Episode] = IO(episode)
    }
    var directory: Option[Episode] = None
    val directoryClient = new LocalMoviesDirectory {
      override def save(episode: Episode): IO[Unit] = IO {
        directory = Some(episode)
      }
    }
    val program = new DownloadMovies(renTvClient, directoryClient)

    // when:
    program.run().unsafeRun

    // then:
    directory must contain(episode)
  }
}
