import IsEq.syntax
import org.scalatest.freespec.AnyFreeSpec
import Homework.IO
import org.scalacheck.Arbitrary
import org.scalacheck.Gen
import org.scalactic.Equivalence
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.Assertions
import org.scalatest.compatible.Assertion
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class MonaLawsTest extends AnyFreeSpec with MonadLaws with VerifyIsEq with Arbitraries {
  "Monad laws" - {
    "Left identity (+1 points for Gryffindor)" in check(leftIdentity _)
    "Right identity (+1 points for Gryffindor)" in check(rightIdentity _)
    "Associativity (+1 points for Gryffindor)" in check(associativity _)
  }
}

trait MonadLaws {

  type A
  type B
  type C

  /** Left identity: return a >>= f ≡ f a */
  def leftIdentity(a: A, f: A => IO[B]): IsEq[IO[B]] = IO(a).flatMap(f) <-> f(a)

  /** Right identity: m >>= return ≡ m */
  def rightIdentity(m: IO[A]): IsEq[IO[A]] = m.flatMap(a => IO(a)) <-> m

  /** Associativity: (m >>= f) >>= g ≡ m >>= (\x -> f x >>= g) */
  def associativity(m: IO[A], f: A => IO[B], g: B => IO[C]): IsEq[IO[C]] =
    m.flatMap(f).flatMap(g) <-> m.flatMap(x => f(x).flatMap(g))
}

case class IsEq[+A](left: A, right: A)

object IsEq {
  implicit class syntax[A](val left: A) extends AnyVal {
    def <->(right: A): IsEq[A] = IsEq(left, right)
  }
}

trait VerifyIsEq extends ScalaCheckPropertyChecks with Assertions with TypeCheckedTripleEquals {
  implicit def ioEquivalence[A: Equivalence]: Equivalence[IO[A]] =
    (a: IO[A], b: IO[A]) =>
      (a.unsafeRun, b.unsafeRun) match {
        case (left, right) if left === right => true
        case (left, right) =>
          println(s"$left is not eq to $right")
          false
      }

  implicit class VerifyIsEqOps[+A: Equivalence](val isEq: IsEq[A]) {
    def verify: Assertion = isEq match {
      case IsEq(left, right) => assert(left === right)
    }
  }

  def check[A: Arbitrary, R: Equivalence](f: (A) => IsEq[R]) = forAll { (a: A) =>
    f(a).verify
  }

  def check[A: Arbitrary, B: Arbitrary, R: Equivalence](f: (A, B) => IsEq[R]) = forAll { (a: A, b: B) =>
    f(a, b).verify
  }

  def check[A: Arbitrary, B: Arbitrary, C: Arbitrary, R: Equivalence](f: (A, B, C) => IsEq[R]) = forAll {
    (a: A, b: B, c: C) =>
      f(a, b, c).verify
  }
}

trait Arbitraries {
  import Arbitrary.arbitrary

  type A = Int
  type B = String
  type C = Boolean

  implicit val arbA: Arbitrary[A]          = Arbitrary.arbInt
  implicit val arbM: Arbitrary[IO[A]]      = Arbitrary(arbitrary[A].map(a => IO(a)))
  implicit val arbF: Arbitrary[A => IO[B]] = Arbitrary(Gen.const(a => IO(s"[$a]")))
  implicit val arbG: Arbitrary[B => IO[C]] = Arbitrary(Gen.const(b => IO(b.substring(1, b.length - 1).toInt > 0)))
}
