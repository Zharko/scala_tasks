import sbt._

object Deps {
  // provide type classes such as Monoid, Functor, Applicative, Monad, and many others
  val cats = "org.typelevel" %% "cats-core" % "2.2.0"

  // library to run unit tests
  val scalatest = "org.scalatest"     %% "scalatest"       % "3.2.2"   % "test"

  // addon for the scalatest to run property base tests with scalacheck
  val scalacheck = "org.scalatestplus" %% "scalacheck-1-15" % "3.2.3.0" % "test"

  // provide instances of type classes from the cats for scalacheck types
  val cats_scalacheck = "io.chrisdavenport" %% "cats-scalacheck" % "0.3.0"   % "test"
}
