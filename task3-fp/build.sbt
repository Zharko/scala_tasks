import Deps._

name := "task3-fp"

version := "0.1"

scalaVersion := "2.13.4"

ThisBuild / organization := "ru.dins.scala-school"

libraryDependencies ++= Seq(
  cats,
  scalatest,
  scalacheck,
  cats_scalacheck
)

lazy val printTestClasspath = taskKey[Unit]("Dump classpath")

printTestClasspath := {
  val classPath = (fullClasspath in Test value)
  val string = classPath.map(_.data.getCanonicalPath).mkString(":")
  new java.io.PrintWriter("classpath.txt") { write(string); close() }
}
