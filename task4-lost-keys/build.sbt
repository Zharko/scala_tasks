
name := "task4-lost-keys"
version := "0.1"
scalaVersion := "2.13.5"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  "-language:postfixOps",
  "-language:higherKinds",
)

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-effect" % "2.3.3",
  "org.typelevel" %% "log4cats-slf4j" % "1.2.0-RC3",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.scalatest" %% "scalatest" % "3.2.2" % Test,
  "org.scalatestplus" %% "scalacheck-1-15" % "3.2.5.0" % Test,
)

addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")

lazy val printTestClasspath = taskKey[Unit]("Dump Test classpath")

printTestClasspath := {
  val classPath = (fullClasspath in Test value)
  val string = classPath.map(_.data.getCanonicalPath).mkString(":")
  new java.io.PrintWriter("classpath.txt") { write(string); close() }
}
