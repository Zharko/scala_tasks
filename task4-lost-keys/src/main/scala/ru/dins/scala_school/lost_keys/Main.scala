package ru.dins.scala_school.lost_keys

import cats.effect.concurrent.Ref
import cats.effect.{ExitCode, IO, IOApp}
import org.typelevel.log4cats.slf4j.Slf4jLogger

import java.util.UUID
import scala.concurrent.duration._
import scala.util.Random

object Main extends IOApp {

  private val logger = Slf4jLogger.getLogger[IO]

  val colours = List(
    Console.RED,
    Console.GREEN,
    Console.YELLOW,
    Console.BLUE,
    Console.MAGENTA,
    Console.CYAN,
    Console.WHITE,
  )

  val genTeams: IO[Set[String]] = IO.delay {
    val nouns = List(
      "fox",
      "rabbit",
      "cat",
      "wolf",
      "dog",
      "chicken",
      "crocodile",
      "kangaroo",
    )

    val adjectives = List(
      "lazy",
      "funny",
      "fluffy",
      "jumpy",
      "tender",
      "smooth",
      "suspicious",
      "sulky",
    )

    (Random.shuffle(adjectives) zip Random.shuffle(nouns)).map { case (a, n) => s"$a $n" }.toSet
  }

  val keysCount           = 512
  val teamsCount          = 3
  val maxValidateDuration = 1000

  val checkKey, changeKey = IO.sleep(Random.nextLong(maxValidateDuration / 4).millis + (maxValidateDuration / 4).millis)

  override def run(args: List[String]): IO[ExitCode] = for {
    teams      <- genTeams.map(_.take(teamsCount))
    teamColour <- IO((teams zip Random.shuffle(colours)).toMap)
    validKey   <- IO(UUID.randomUUID())
    keys       <- IO(Random.shuffle((1 until keysCount).map(_ => UUID.randomUUID()) :+ validKey))
    timeout = (keysCount * maxValidateDuration / teamsCount).millis
    deadline    <- IO(timeout.fromNow)
    keysCounter <- Ref.of[IO, Int](keysCount)

    result <- Cage[IO, String, UUID]
      .escape(
        lock = (team: String, i: UUID) =>
          for {
            colour <- IO(teamColour(team))
            _      <- logProgress(team, colour, deadline, s"is trying key $i,")
            _      <- checkKey
            result = i == validKey
            keysLeft <- keysCounter.updateAndGet(_ - 1)
            _ <- result match {
              case true  => logProgress(team, colour, deadline, s"succeed! $keysLeft and")
              case false => logProgress(team, colour, deadline, s"failed! $keysLeft and")
            }
            _ <- changeKey
          } yield result,
        keys = keys.toSet,
        teams = teams,
      )
      .timeout(timeout)
      .attempt
    winner = result match {
      case Right((_, winner, _)) => winner
      case Left(_)               => None
    }
    timeLeft <- IO(deadline.timeLeft.toSeconds)
    _ <- winner match {
      case Some(team) => logger.info(s"${teamColour(team)}Finally $team wins! $timeLeft seconds left.${Console.RESET}")
      case None       => logger.info(s"Everyone loses!")
    }
  } yield ExitCode.Success

  val timeAdverbs = List(
    "Meanwhile",
    "Meantime",
    "Simultaneously",
    "In the the intervening time",
    "At the same time",
    "Concurrently",
    "At the same instant",
    "At the same moment",
  )

  def logProgress(team: String, colour: String, deadline: Deadline, progress: String): IO[Unit] = for {
    timeLeft <- IO(deadline.timeLeft.toSeconds)
    adverb   <- IO(Random.shuffle(timeAdverbs).head)
    _        <- logger.info(s"$colour$adverb $team $progress $timeLeft seconds left.${Console.RESET}")
  } yield ()
}
