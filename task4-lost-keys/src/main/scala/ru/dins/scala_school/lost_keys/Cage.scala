package ru.dins.scala_school.lost_keys

import cats.Parallel
import cats.effect.Concurrent
import cats.effect.concurrent.{Ref, Semaphore}
import cats.syntax.all._

/** Логика конкуррентного поиска ключей несколькими командами
  *
  * @tparam F тип эффекта
  * @tparam Team тип команды
  * @tparam Key тип ключа
  */
trait Cage[F[_], Team, Key] {

  /** Функция принимает на вход набор ключей, команд и функцию отпирающую клетку.
    * Основные правила:
    *  1. на каждую команду существует один замок и в рамках команды ключи перебираются последовательно
    *  2. у каждой команды свой собственный замок и команды могут и должны перебирать ключи параллельно
    *  3. каждый ключ проверяется один и только один раз как внутри одной команды так и между командами
    *  4. после проверки вне зависимости от результата каждый ключ остаётся у проверяющей команды
    *
    * @param lock функция отпирания клетки
    * @param keys изначальный набор ключей
    * @param teams команды пытающиеся выбраться из клетки
    * @return кортеж из 3х элементов
    *         1. "оставшиеся" ключи - ключи не проверенные ни одной командой
    *         2. опционально, команда нашедшая ключ
    *         3. соответствие команды и всех перебранных ею ключей
    */
  def escape(
      lock: (Team, Key) => F[Boolean],
      keys: Set[Key],
      teams: Set[Team],
  ): F[(Set[Key], Option[Team], Map[Team, Set[Key]])]
}

class CageImpl[F[_]: Concurrent: Parallel, Team, Key] extends Cage[F, Team, Key] {
  override def escape(
      lock: (Team, Key) => F[Boolean],
      keys: Set[Key],
      teams: Set[Team],
  ): F[(Set[Key], Option[Team], Map[Team, Set[Key]])] = for {

    sem <- Semaphore[F](1)

    winnerRef: Ref[F, Option[Team]] <- Ref.of[F, Option[Team]](None)
    sharedKeys: Ref[F, Set[Key]]    <- Ref.of[F, Set[Key]](keys)
    checkedKeys: Set[Key] = Set.empty

    attempts <- teams.toList.parTraverse { team =>
      check(sem, lock, team, sharedKeys, winnerRef, checkedKeys)
    }

    leftKeys <- sharedKeys.get
    winner   <- winnerRef.get

  } yield (leftKeys, winner, attempts.toMap)

  def check(
      sem: Semaphore[F],
      lock: (Team, Key) => F[Boolean],
      currentTeam: Team,
      leftKeys: Ref[F, Set[Key]],
      winner: Ref[F, Option[Team]],
      checkedKeys: Set[Key],
  ): F[(Team, Set[Key])] =
    for {
      keyToCheck: Option[Key] <- sem.withPermit(for {
        keys       <- leftKeys.get
        winnerTeam <- winner.get
        key <-
          if (keys.isEmpty || winnerTeam.isDefined) Option.empty[Key].pure[F]
          else leftKeys.set(keys.drop(1)).as(keys.headOption)
      } yield key)

      correctKey <- keyToCheck match {
        case Some(key) => lock(currentTeam, key)
        case None      => Concurrent[F].pure(false)
      }

      winnerTeam <- winner.modify(old => (if (old.isEmpty && correctKey) Some(currentTeam) else old, old))

      result <-
        if (winnerTeam.isDefined || keyToCheck.isEmpty) {
          Concurrent[F].pure((currentTeam, updateKeys(checkedKeys, keyToCheck)))
        } else {
          check(sem, lock, currentTeam, leftKeys, winner, updateKeys(checkedKeys, keyToCheck))
        }
    } yield result

  def updateKeys(keys: Set[Key], currentKey: Option[Key]): Set[Key] =
    if (currentKey.isDefined) {
      keys + currentKey.head
    } else {
      keys
    }
}

object Cage {

  /** Метод возвращающий реализованную Вами логику
    *
    * Вам предстоит самим выбрать ограничения контекста эффекта F[_] в зависимости от выбранного подхода для решения
    */
  def apply[F[_]: Concurrent: Parallel, Team, Key]: Cage[F, Team, Key] = new CageImpl[F, Team, Key]
}
