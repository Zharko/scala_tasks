package ru.dins.scala_school.lost_keys

import cats.effect.IO
import cats.implicits._
import org.scalacheck.Gen
import org.scalacheck.Gen.{choose, sized}
import org.scalatest.Assertion
import org.scalatest.OptionValues.convertOptionToValuable
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt
import scala.util.Random

class CageSpec extends AnyFlatSpec with ScalaCheckDrivenPropertyChecks with Matchers {

  implicit val timer        = IO.timer(ExecutionContext.global)
  implicit val contextShift = IO.contextShift(ExecutionContext.global)

  val keysGen  = Gen.nonEmptyContainerOf[Set, String](Gen.numStr)
  val teamsGen = sized(_ => choose(1, 16)).flatMap(Gen.containerOfN[Set, String](_, Gen.alphaStr))

  it should "return all keys if succeed" in {
    forAll(keysGen, teamsGen) { (keys: Set[String], teams: Set[String]) =>
      check {
        for {
          validKey <- IO(Random.shuffle(keys).head)
          (leftKeys, winner, teamKeys) <- Cage[IO, String, String].escape(
            (_, key) => IO(key == validKey),
            keys,
            teams,
          )
        } yield {
          winner should not be empty
          teamKeys.values.fold(leftKeys)(_ ++ _) shouldBe keys
        }
      }
    }
  }

  it should "collect valid key by winner" in {
    forAll(keysGen, teamsGen) { (keys: Set[String], teams: Set[String]) =>
      check {
        for {
          validKey              <- IO(Random.shuffle(keys).head)
          (_, winner, teamKeys) <- Cage[IO, String, String].escape((_, key) => IO(key == validKey), keys, teams)
        } yield {
          winner should not be empty
          teamKeys(winner.value) should contain(validKey)
        }
      }
    }
  }

  it should "return all keys if failed" in {
    forAll(keysGen, teamsGen) { (keys: Set[String], teams: Set[String]) =>
      check {
        for {
          (leftKeys, winner, teamKeys) <- Cage[IO, String, String].escape((_, _) => false.pure[IO], keys, teams)
        } yield {
          leftKeys shouldBe Set()
          winner shouldBe empty
          teamKeys.values.fold(leftKeys)(_ ++ _) shouldBe keys
        }
      }
    }
  }

  def check(f: IO[Assertion]): Assertion = f.timeout(3.seconds).unsafeRunSync

}
