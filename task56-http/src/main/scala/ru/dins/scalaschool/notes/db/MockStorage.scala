package ru.dins.scalaschool.notes.db

import cats.Applicative
import ru.dins.scalaschool.notes.Storage
import ru.dins.scalaschool.notes.models.Models.{ApiError, Note}

import java.util.UUID

case class MockStorage[F[_]: Applicative]() extends Storage[F] {
  override def post(name: String, text: String): F[Either[ApiError, Note]] =
    Applicative[F].pure(Right(Note.fake.copy(name = name, text = text)))

  override def get(id: UUID): F[Either[ApiError, Note]] =
    Applicative[F].pure(Right(Note.fake.copy(id = id)))

  override def delete(id: UUID): F[Either[ApiError, Unit]] = Applicative[F].pure(Right((): Unit))

  override def patch(id: UUID, name: Option[String], text: Option[String]): F[Either[ApiError, Note]] =
    Applicative[F].pure(Right(Note.fake))

  override def archive(id: UUID, isArchived: Boolean): F[Either[ApiError, Note]] =
    Applicative[F].pure(Right(Note.fake))

  override def getAll(
      name: Option[String],
      text: Option[String],
      isArchived: Option[Boolean],
      orderBy: Option[String],
      orderDir: Option[String],
  ): F[Either[ApiError, List[Note]]] = {
    val notes: List[Note] = List(Note.fake, Note.yaFake)
    Applicative[F].pure(Right(notes))
  }
}
