package ru.dins.scalaschool.notes.models

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import java.time.LocalDateTime
import java.util.UUID

object Models {
  case class Note(
      id: UUID,
      name: String,
      text: String,
      createdAt: LocalDateTime,
      isArchived: Boolean,
  )
  object Note {
    implicit val codec: Codec[Note] = deriveCodec[Note]

    def fake: Note = {
      val id   = UUID.fromString("00000000-0000-0000-0000-000000000000")
      val name = "Greek Philosophers"
      val text = "Socrates, Platon, Aristotle"
      val arch = false
      val date = LocalDateTime.parse("-999999999-01-01T00:00:00")
      Note(id, name, text, date, arch)
    }

    def yaFake: Note = {
      val id   = UUID.fromString("00000000-0000-0000-0000-000000000001")
      val name = "Solar System"
      val text = "Mercury, Venus, Earth, Mars, Jupiter, Saturn, Neptune, (Pluto?)"
      val arch = true
      val date = LocalDateTime.parse("-999999999-01-01T00:00:00")
      Note(id, name, text, date, arch)
    }
  }

  case class SimpleNote(name: String, text: String)
  object SimpleNote {
    implicit val codec: Codec[SimpleNote] = deriveCodec[SimpleNote]
  }

  case class SimpleNoteOption(name: Option[String], text: Option[String])
  object SimpleNoteOption {
    implicit val codec: Codec[SimpleNoteOption] = deriveCodec[SimpleNoteOption]
  }

  case class ApiError(code: Int, message: String)
  object ApiError {
    implicit val codec: Codec[ApiError]    = deriveCodec[ApiError]
    val Unexpected: ApiError               = ApiError(500, "Unexpected error")
    val NotFound: ApiError                 = ApiError(404, "Not found")
    def NotFound(id: UUID): ApiError       = ApiError(404, s"Note with id=$id not found")
    def LogicError(name: String): ApiError = ApiError(422, s"Name $name is already taken")
    val BadRequest: ApiError               = ApiError(400, "Malformed message body: Invalid JSON")
  }
}
