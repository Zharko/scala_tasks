// Uncomment for Task 6
// Это миграции для БД. Необходимо выполнять миграции на страте приложения
// Запрос на создание таблицы приведен для примера и должен быть доработан

package ru.dins.scalaschool.notes.db

import cats.effect.Sync
import doobie.implicits._
import cats.implicits._
import doobie.util.transactor.Transactor.Aux

object Migrations {

  private val uuid = sql"""create extension if not exists "uuid-ossp";""".update
  private val migration =
    sql"""
         create table if not exists notes(
          id uuid not null default uuid_generate_v4(),
          name text not null unique,
          text text not null,
          created_at timestamp not null default now(),
          is_archived boolean not null default false
         );
       """.update

  def migrate[F[_]: Sync](xa: Aux[F, Unit]): F[Unit] = (uuid.run, migration.run).mapN(_ + _).void.transact(xa)

}
