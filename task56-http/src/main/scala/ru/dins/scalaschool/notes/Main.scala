package ru.dins.scalaschool.notes

import cats.effect.{ExitCode, IO, IOApp, Resource}
import doobie.util.transactor.Transactor
import org.http4s.implicits.http4sKleisliResponseSyntaxOptionT
import org.http4s.server.{Router, Server}
import ru.dins.scalaschool.notes.db.{Migrations, PostgresStorage}
import org.http4s.server.blaze.BlazeServerBuilder

import scala.concurrent.ExecutionContext

object Main extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val xa = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver",
      "jdbc:postgresql:postgres",
      "postgres",
      "postgres",
    )
    Migrations.migrate(xa).unsafeRunSync()

    //val service    = MockStorage[IO]()
    val servicePostgres = PostgresStorage[IO](xa)
    val controller      = Controller(servicePostgres)

    val httpApp = Router(
      "/api" -> controller.routes,
    ).orNotFound

    // Создайте здесь свой сервер
    val server: Resource[IO, Server[IO]] = BlazeServerBuilder[IO](ExecutionContext.global)
      .bindHttp(8080, "0.0.0.0")
      .withHttpApp(httpApp)
      .resource
    server.use(_ => IO.never).as(ExitCode.Success)
  }
}
