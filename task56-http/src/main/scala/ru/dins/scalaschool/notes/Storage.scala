package ru.dins.scalaschool.notes

import ru.dins.scalaschool.notes.models.Models.{ApiError, Note}

import java.util.UUID

trait Storage[F[_]] {
  def post(name: String, text: String): F[Either[ApiError, Note]]

  def get(id: UUID): F[Either[ApiError, Note]]

  def delete(id: UUID): F[Either[ApiError, Unit]]

  def patch(id: UUID, name: Option[String], text: Option[String]): F[Either[ApiError, Note]]

  def archive(id: UUID, isArchived: Boolean): F[Either[ApiError, Note]]

  def getAll(
      name: Option[String],
      text: Option[String],
      isArchived: Option[Boolean],
      orderBy: Option[String],
      orderDir: Option[String],
  ): F[Either[ApiError, List[Note]]]
}
