package ru.dins.scalaschool.notes

import cats.effect.Sync
import org.http4s.{HttpRoutes, MessageFailure, Response}
import org.http4s.dsl.Http4sDsl
import ru.dins.scalaschool.notes.models.Models.{ApiError, SimpleNote, SimpleNoteOption}
import org.http4s.circe.CirceEntityCodec._
import cats.syntax.all._
import io.circe.Encoder

case class Controller[F[_]: Sync](storage: Storage[F]) extends Http4sDsl[F] {
  object ArchiveParam extends QueryParamDecoderMatcher[Boolean]("isArchived")

  object NameOption    extends OptionalQueryParamDecoderMatcher[String]("name")
  object TextOption    extends OptionalQueryParamDecoderMatcher[String]("text")
  object ArchiveOption extends OptionalQueryParamDecoderMatcher[Boolean]("isArchived")
  object SortOption    extends OptionalQueryParamDecoderMatcher[String]("orderBy")
  object OrderOption   extends OptionalQueryParamDecoderMatcher[String]("orderDir")

  def routes: HttpRoutes[F] =
    HttpRoutes.of[F] {
      case request @ POST -> Root / "note" =>
        val payload = request.as[SimpleNote]
        handle(payload.flatMap(note => storage.post(note.name, note.text)))

      case GET -> Root / "note" / UUIDVar(id) => handle(storage.get(id))

      case DELETE -> Root / "note" / UUIDVar(id) => handle(storage.delete(id))

      case request @ PATCH -> Root / "note" / UUIDVar(id) =>
        val payload = request.as[SimpleNoteOption]
        handle(payload.flatMap(note => storage.patch(id, note.name, note.text)))

      case POST -> Root / "note" / UUIDVar(id) :? ArchiveParam(arch) => handle(storage.archive(id, arch))

      case GET -> Root / "note" / "list" :?
          NameOption(name) +&
          TextOption(text) +&
          ArchiveOption(arch) +&
          SortOption(param) +&
          OrderOption(order) =>
        handle(storage.getAll(name, text, arch, param, order))

      case _ => NotFound(ApiError.NotFound)
    }

  private def handle[A: Encoder](f: F[Either[ApiError, A]]): F[Response[F]] =
    f.flatMap {
      case e @ Left(ApiError(404, _)) => NotFound(e.value)
      case Left(ApiError(304, _))     => NotModified()
      case e @ Left(_)                => UnprocessableEntity(e.value)
      case Right(note)                => Ok(note)
    }.handleErrorWith {
      case _: MessageFailure => BadRequest(ApiError.BadRequest)
      case _                 => InternalServerError(ApiError.Unexpected)
    }
}
