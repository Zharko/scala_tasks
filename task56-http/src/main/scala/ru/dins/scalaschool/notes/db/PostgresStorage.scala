// Uncomment for Task 6

// Здесь будет содержаться логика приложения. В зависимости от параметров HTTP запросов нужно
// по-разному обращаться к базе, обрабатывать ошибки, фильтровать результаты и т.д

package ru.dins.scalaschool.notes.db

import cats.effect.Sync
import cats.syntax.all._
import doobie.ConnectionIO
import doobie.implicits._
import doobie.util.query.Query0
import doobie.postgres.implicits._
import doobie.postgres.sqlstate.class23.UNIQUE_VIOLATION
import doobie.util.fragments.{setOpt, whereAndOpt}
import doobie.util.transactor.Transactor.Aux
import doobie.util.update.Update0
import ru.dins.scalaschool.notes.Storage
import ru.dins.scalaschool.notes.models.Models.{ApiError, Note}

import java.util.UUID

object Queries {
  def getQuery(id: UUID): Query0[Note] =
    sql"""select * from notes where id = $id""".query[Note]
  def insertQuery(name: String, text: String): Update0 =
    sql"""insert into notes (name, text) values ($name, $text)""".update
  def deleteQuery(id: UUID): Update0 =
    sql"""delete from notes where id = $id""".update
  def updateQuery(id: UUID, name: Option[String], text: Option[String]): Update0 = {
    val nameOpt = name.map(name => fr"name=$name")
    val textOpt = text.map(text => fr"text=$text")

    val update = fr"update notes"
    (update ++ setOpt(nameOpt, textOpt) ++ fr"where id=$id").update
  }
  def archiveQuery(id: UUID, isArchived: Boolean): Update0 =
    sql"""update notes set is_archived = $isArchived where id = $id""".update
  def getAllQuery(
      name: Option[String],
      text: Option[String],
      isArchived: Option[Boolean],
      orderBy: Option[String],
      orderDir: Option[String],
  ): ConnectionIO[List[Note]] = {
    val nameOpt = name.map(name => fr"name ilike '%' || $name || '%'")
    val textOpt = text.map(text => fr"text ilike '%' || $text || '%'")
    val archOpt = isArchived.map(arch => fr"is_archived=$arch")
    val select  = fr"select * from notes"

    (select ++ whereAndOpt(nameOpt, textOpt, archOpt) ++
      (orderBy match {
        case Some(name) if name == "name"       => fr"order by name"
        case Some(text) if text == "text"       => fr"order by text"
        case Some(arch) if arch == "isArchived" => fr"order by is_archived"
        case Some(date) if date == "createdAt"  => fr"order by created_at"
        case _                                  => fr"order by name"
      }) ++
      (orderDir match {
        case Some(orderDir) if orderDir == "desc" => fr"desc"
        case _                                    => fr"asc"
      })).query[Note].to[List]
  }
}

case class PostgresStorage[F[_]: Sync](xa: Aux[F, Unit]) extends Storage[F] {
  override def post(name: String, text: String): F[Either[ApiError, Note]] =
    Queries
      .insertQuery(name, text)
      .withUniqueGeneratedKeys[Note]("id", "name", "text", "created_at", "is_archived")
      .attemptSql
      .map {
        case Left(_)     => Left(ApiError.LogicError(name))
        case Right(note) => Right(note)
      }
      .transact(xa)

  override def get(id: UUID): F[Either[ApiError, Note]] =
    Queries
      .getQuery(id)
      .option
      .map {
        case None       => Left(ApiError.NotFound(id))
        case Some(note) => Right(note)
      }
      .transact(xa)

  override def delete(id: UUID): F[Either[ApiError, Unit]] =
    Queries.deleteQuery(id).run.transact(xa).map(_ => Right((): Unit))

  override def patch(id: UUID, name: Option[String], text: Option[String]): F[Either[ApiError, Note]] =
    if (name.isEmpty && text.isEmpty) {
      Queries
        .getQuery(id)
        .option
        .map {
          case None => Left(ApiError.NotFound(id)).withRight[Note]
          case _    => Left(ApiError(304, "Not Modified")).withRight[Note]
        }
        .transact(xa)
    } else {
      Queries
        .updateQuery(id, name, text)
        .withGeneratedKeys[Note]("id", "name", "text", "created_at", "is_archived")
        .compile
        .toList
        .attemptSomeSqlState { case UNIQUE_VIOLATION =>
          ApiError.LogicError(name.head)
        }
        .map {
          case Right(notes) if notes.isEmpty => Left(ApiError.NotFound(id))
          case Right(notes)                  => Right(notes.head)
          case Left(error: ApiError)         => Left(error)
        }
        .transact(xa)
    }

  override def archive(id: UUID, isArchived: Boolean): F[Either[ApiError, Note]] =
    Queries
      .archiveQuery(id, isArchived)
      .withGeneratedKeys[Note]("id", "name", "text", "created_at", "is_archived")
      .compile
      .toList
      .map {
        case note if note.isEmpty => Left(ApiError.NotFound(id))
        case note                 => Right(note.head)
      }
      .transact(xa)

  override def getAll(
      name: Option[String],
      text: Option[String],
      isArchived: Option[Boolean],
      orderBy: Option[String],
      orderDir: Option[String],
  ): F[Either[ApiError, List[Note]]] =
    Queries.getAllQuery(name, text, isArchived, orderBy, orderDir).transact(xa).map(list => Right(list))

}
