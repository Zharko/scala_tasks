// Uncomment for task 6

package ru.dins.scalaschool.notes.db

import cats.effect.{ContextShift, IO}
import com.dimafeng.testcontainers.PostgreSQLContainer
import com.dimafeng.testcontainers.scalatest.TestContainerForAll
import doobie.Transactor
import doobie.implicits._
import doobie.util.transactor.Transactor.Aux
import org.scalamock.scalatest.MockFactory
import org.scalatest.Assertion
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import ru.dins.scalaschool.notes.Storage
import ru.dins.scalaschool.notes.models.Models.{ApiError, Note}
import doobie.postgres.implicits._

import java.time.LocalDateTime
import java.util.UUID
import scala.concurrent.ExecutionContext

class PostgresStorageTest extends AnyFlatSpec with Matchers with TestContainerForAll with MockFactory {
  implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

  override val containerDef: PostgreSQLContainer.Def = PostgreSQLContainer.Def()

  def createTransactor(container: Containers): Aux[IO, Unit] =
    Transactor.fromDriverManager[IO](
      driver = "org.postgresql.Driver",
      url = container.jdbcUrl,
      user = container.username,
      pass = container.password,
    )

  override def startContainers(): PostgreSQLContainer = {
    val container = super.startContainers()
    val xa        = createTransactor(container)
    Migrations.migrate(xa).unsafeRunSync()
    container
  }

  // Чистим таблицу перед каждым тестом
  def resetStorage(test: (Storage[IO], Transactor.Aux[IO, Unit]) => IO[Assertion]): Unit =
    withContainers { container =>
      val xa       = createTransactor(container)
      val truncate = sql"truncate notes".update.run
      val storage  = PostgresStorage(xa)

      val result = for {
        _         <- truncate.transact(xa)
        assertion <- test(storage, xa)
      } yield assertion

      result.unsafeRunSync()
    }

  val fakeNote: Note = {
    val id: UUID            = UUID.fromString("00000000-0000-0000-0000-00000000000")
    val name: String        = "Greek Philosophers"
    val text: String        = "Socrates, Platon, Aristotle"
    val date                = LocalDateTime.parse("-999999999-01-01T00:00:00")
    val isArchived: Boolean = false

    Note(id, name, text, date, isArchived)
  }
  val yaFakeNote: Note = {
    val id         = UUID.fromString("00000000-0000-0000-0000-000000000001")
    val name       = "Solar System"
    val text       = "Mercury, Venus, Earth, Mars, Jupiter, Saturn, Neptune, (Pluto?)"
    val date       = LocalDateTime.parse("-999999999-01-01T00:00:00")
    val isArchived = true

    Note(id, name, text, date, isArchived)
  }

  "POST" should "return new note with provided name and text" in resetStorage { case (storage, xa) =>
    val getQuery = sql"""select * from notes where name = ${fakeNote.name}""".query[Note]

    for {
      sample <- storage.post(fakeNote.name, fakeNote.text)
      note   <- getQuery.unique.transact(xa)
    } yield sample shouldBe Right(note)
  }

  it should "return error if note with provided name is already exists" in resetStorage { case (storage, xa) =>
    val insert =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertResult = insert.transact(xa)

    for {
      _      <- insertResult
      sample <- storage.post(fakeNote.name, fakeNote.text)
    } yield sample shouldBe Left(ApiError.LogicError(fakeNote.name))
  }

  "GET" should "return Note if storage is not empty" in resetStorage { case (storage, xa) =>
    val insert =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertResult = insert.transact(xa)

    for {
      _      <- insertResult
      sample <- storage.get(fakeNote.id)
    } yield sample shouldBe Right(fakeNote)
  }

  it should "return error if storage is empty" in resetStorage { case (storage, xa) =>
    for {
      sample <- storage.get(fakeNote.id)
    } yield sample shouldBe Left(ApiError.NotFound(fakeNote.id))
  }

  "DELETE" should "return empty body if note has been deleted" in resetStorage { case (storage, xa) =>
    val insert =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertResult = insert.transact(xa)

    for {
      _      <- insertResult
      sample <- storage.delete(fakeNote.id)
    } yield sample shouldBe Right((): Unit)
  }

  it should "return empty body if storage is empty" in resetStorage { case (storage, xa) =>
    for {
      sample <- storage.delete(fakeNote.id)
    } yield sample shouldBe Right((): Unit)
  }

  "PATCH" should "return updated note with provided name and text if storage is not empty" in resetStorage {
    case (storage, xa) =>
      val insert =
        sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
      val insertResult = insert.transact(xa)

      for {
        _      <- insertResult
        sample <- storage.patch(fakeNote.id, Some(yaFakeNote.name), Some(yaFakeNote.text))
      } yield sample shouldBe Right(
        Note(fakeNote.id, yaFakeNote.name, yaFakeNote.text, fakeNote.createdAt, fakeNote.isArchived),
      )
  }

  it should "return updated note with provided name if storage is not empty" in resetStorage { case (storage, xa) =>
    val insert =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertResult = insert.transact(xa)

    for {
      _      <- insertResult
      sample <- storage.patch(fakeNote.id, Some(yaFakeNote.name), None)
    } yield sample shouldBe Right(
      Note(fakeNote.id, yaFakeNote.name, fakeNote.text, fakeNote.createdAt, fakeNote.isArchived),
    )
  }

  it should "return updated note with provided text if storage is not empty" in resetStorage { case (storage, xa) =>
    val insert =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertResult = insert.transact(xa)

    for {
      _      <- insertResult
      sample <- storage.patch(fakeNote.id, None, Some(yaFakeNote.text))
    } yield sample shouldBe Right(
      Note(fakeNote.id, fakeNote.name, yaFakeNote.text, fakeNote.createdAt, fakeNote.isArchived),
    )
  }

  it should "return not modified error if storage is not empty" in resetStorage { case (storage, xa) =>
    val insert =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertResult = insert.transact(xa)

    for {
      _      <- insertResult
      sample <- storage.patch(fakeNote.id, None, None)
    } yield sample shouldBe Left(ApiError(304, "Not Modified"))
  }

  it should "return error if storage is empty and name&text parameters were provided" in resetStorage {
    case (storage, xa) =>
      for {
        sample <- storage.patch(fakeNote.id, Some(yaFakeNote.name), Some(yaFakeNote.text))
      } yield sample shouldBe Left(ApiError.NotFound(fakeNote.id))
  }

  it should "return error if storage is empty and only name parameter was provided" in resetStorage {
    case (storage, xa) =>
      for {
        sample <- storage.patch(fakeNote.id, Some(yaFakeNote.name), None)
      } yield sample shouldBe Left(ApiError.NotFound(fakeNote.id))
  }

  it should "return error if storage is empty and only text parameter was provided" in resetStorage {
    case (storage, xa) =>
      for {
        sample <- storage.patch(fakeNote.id, None, Some(yaFakeNote.text))
      } yield sample shouldBe Left(ApiError.NotFound(fakeNote.id))
  }

  it should "return error if storage is empty and none of the parameters were provided" in resetStorage {
    case (storage, xa) =>
      for {
        sample <- storage.patch(fakeNote.id, None, None)
      } yield sample shouldBe Left(ApiError.NotFound(fakeNote.id))
  }

  "POST" should "return archived note if storage is not empty" in resetStorage { case (storage, xa) =>
    val insert =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertResult = insert.transact(xa)

    for {
      _      <- insertResult
      sample <- storage.archive(fakeNote.id, isArchived = true)
    } yield sample shouldBe Right(
      Note(fakeNote.id, fakeNote.name, fakeNote.text, fakeNote.createdAt, isArchived = true),
    )
  }

  it should "return unarchived note if storage is not empty" in resetStorage { case (storage, xa) =>
    val insert =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${true})""".update.run
    val insertResult = insert.transact(xa)

    for {
      _      <- insertResult
      sample <- storage.archive(fakeNote.id, isArchived = false)
    } yield sample shouldBe Right(
      Note(fakeNote.id, fakeNote.name, fakeNote.text, fakeNote.createdAt, isArchived = false),
    )
  }

  it should "return error if storage is empty" in resetStorage { case (storage, xa) =>
    for {
      sample <- storage.archive(fakeNote.id, isArchived = true)
    } yield sample shouldBe Left(ApiError.NotFound(fakeNote.id))
  }

  "GET" should "return list of notes if storage is not empty" in resetStorage { case (storage, xa) =>
    val insertNote =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertYaNote =
      sql"""insert into notes values (${yaFakeNote.id}, ${yaFakeNote.name}, ${yaFakeNote.text},  ${yaFakeNote.createdAt}, ${yaFakeNote.isArchived})""".update.run
    val insertResult   = insertNote.transact(xa)
    val yaInsertResult = insertYaNote.transact(xa)

    for {
      _      <- insertResult
      _      <- yaInsertResult
      sample <- storage.getAll(None, None, None, None, None)
    } yield sample shouldBe Right(List(fakeNote, yaFakeNote))
  }

  it should "return list of notes sorted by name if storage is not empty" in resetStorage { case (storage, xa) =>
    val insertNote =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertYaNote =
      sql"""insert into notes values (${yaFakeNote.id}, ${yaFakeNote.name}, ${yaFakeNote.text},  ${yaFakeNote.createdAt}, ${yaFakeNote.isArchived})""".update.run
    val insertResult   = insertNote.transact(xa)
    val yaInsertResult = insertYaNote.transact(xa)

    for {
      _      <- insertResult
      _      <- yaInsertResult
      sample <- storage.getAll(Some(yaFakeNote.name), None, None, None, None)
    } yield sample shouldBe Right(List(yaFakeNote))
  }

  it should "return list of notes sorted by text if storage is not empty" in resetStorage { case (storage, xa) =>
    val insertNote =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertYaNote =
      sql"""insert into notes values (${yaFakeNote.id}, ${yaFakeNote.name}, ${yaFakeNote.text},  ${yaFakeNote.createdAt}, ${yaFakeNote.isArchived})""".update.run
    val insertResult   = insertNote.transact(xa)
    val yaInsertResult = insertYaNote.transact(xa)

    for {
      _      <- insertResult
      _      <- yaInsertResult
      sample <- storage.getAll(None, Some(fakeNote.text), None, None, None)
    } yield sample shouldBe Right(List(fakeNote))
  }

  it should "return list of notes sorted by archived parameter if storage is not empty" in resetStorage {
    case (storage, xa) =>
      val insertNote =
        sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${true})""".update.run
      val insertYaNote =
        sql"""insert into notes values (${yaFakeNote.id}, ${yaFakeNote.name}, ${yaFakeNote.text},  ${yaFakeNote.createdAt}, ${false})""".update.run
      val insertResult   = insertNote.transact(xa)
      val yaInsertResult = insertYaNote.transact(xa)

      for {
        _      <- insertResult
        _      <- yaInsertResult
        sample <- storage.getAll(None, None, isArchived = Some(true), None, None)
      } yield sample shouldBe Right(
        List(Note(fakeNote.id, fakeNote.name, fakeNote.text, fakeNote.createdAt, isArchived = true)),
      )
  }

  it should "return empty list of notes if storage is empty" in resetStorage { case (storage, xa) =>
    for {
      sample <- storage.getAll(Some(yaFakeNote.name), None, None, None, None)
    } yield sample shouldBe Right(List())
  }

  it should "return list of notes desc ordered if storage is not empty" in resetStorage { case (storage, xa) =>
    val insertNote =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertYaNote =
      sql"""insert into notes values (${yaFakeNote.id}, ${yaFakeNote.name}, ${yaFakeNote.text},  ${yaFakeNote.createdAt}, ${yaFakeNote.isArchived})""".update.run
    val insertResult   = insertNote.transact(xa)
    val yaInsertResult = insertYaNote.transact(xa)

    for {
      _      <- insertResult
      _      <- yaInsertResult
      sample <- storage.getAll(None, None, None, None, Some("desc"))
    } yield sample shouldBe Right(List(yaFakeNote, fakeNote))
  }

  it should "return list of notes asc ordered if storage is not empty" in resetStorage { case (storage, xa) =>
    val insertNote =
      sql"""insert into notes values (${fakeNote.id}, ${fakeNote.name}, ${fakeNote.text},  ${fakeNote.createdAt}, ${fakeNote.isArchived})""".update.run
    val insertYaNote =
      sql"""insert into notes values (${yaFakeNote.id}, ${yaFakeNote.name}, ${yaFakeNote.text},  ${yaFakeNote.createdAt}, ${yaFakeNote.isArchived})""".update.run
    val insertResult   = insertNote.transact(xa)
    val yaInsertResult = insertYaNote.transact(xa)

    for {
      _      <- insertResult
      _      <- yaInsertResult
      sample <- storage.getAll(None, None, None, None, Some("asc"))
    } yield sample shouldBe Right(List(fakeNote, yaFakeNote))
  }

}
