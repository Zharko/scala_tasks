package ru.dins.scalaschool.notes

import cats.effect.IO
import io.circe.Json
import io.circe.literal.JsonStringContext
import org.http4s.circe.CirceEntityCodec._
import org.http4s.implicits._
import io.circe.syntax.EncoderOps
import org.http4s.{Method, Request, Status, Uri}
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import ru.dins.scalaschool.notes.models.Models.{ApiError, Note, SimpleNote, SimpleNoteOption}

import java.time.LocalDateTime
import java.util.UUID

class ControllerTest extends AnyFlatSpec with Matchers with MockFactory {

  val mockStorage: Storage[IO]   = mock[Storage[IO]]
  val controller: Controller[IO] = Controller(mockStorage)

  def run(
      method: Method,
      path: String,
      body: Option[Json] = None,
      queryMap: Map[String, String] = Map[String, String](),
  ): (Status, Json) = {
    val base = Request[IO](method, Uri(path = path).withQueryParams(queryMap))
    val request = body match {
      case Some(value) => base.withEntity(value)
      case None        => base
    }
    controller.routes.orNotFound
      .run(request)
      .flatMap { response =>
        response.as[Json].map { json => // Attention! It will fail on empty body
          (response.status, json)
        }
      }
      .unsafeRunSync()
  }

  val fakeNote: Note = {
    val id: UUID            = UUID.fromString("00000000-0000-0000-0000-00000000000")
    val name: String        = "Greek Philosophers"
    val text: String        = "Socrates, Platon, Aristotle"
    val date                = LocalDateTime.parse("-999999999-01-01T00:00:00")
    val isArchived: Boolean = false

    Note(id, name, text, date, isArchived)
  }
  val yaFakeNote: Note = {
    val id         = UUID.fromString("00000000-0000-0000-0000-000000000001")
    val name       = "Solar System"
    val text       = "Mercury, Venus, Earth, Mars, Jupiter, Saturn, Neptune, (Pluto?)"
    val date       = LocalDateTime.parse("-999999999-01-01T00:00:00")
    val isArchived = true

    Note(id, name, text, date, isArchived)
  }
  val fakeNoteList: List[Note]               = List(fakeNote, yaFakeNote)
  val fakeSimpleNote: SimpleNote             = SimpleNote(fakeNote.name, fakeNote.text)
  val fakeSimpleNoteOption: SimpleNoteOption = SimpleNoteOption(Some(fakeNote.name), Some(fakeNote.text))

  val testInvalidJson: Json = json""" { "something": "something", "wrong": "wrong" } """
  val emptyResponse: Json   = json"""{}"""

  val expectedJson: Json     = json"""{ "id" : "00000000-0000-0000-0000-000000000000",
                                    "name" : "Greek Philosophers",
                                    "text" : "Socrates, Platon, Aristotle",
                                    "createdAt" : "-999999999-01-01T00:00:00",
                                    "isArchived" : false }"""
  val expectedListJson: Json = json"""[{ "id" : "00000000-0000-0000-0000-000000000000",
                                        "name" : "Greek Philosophers",
                                        "text" : "Socrates, Platon, Aristotle",
                                        "createdAt" : "-999999999-01-01T00:00:00",
                                        "isArchived" : false },
                                      { "id" : "00000000-0000-0000-0000-000000000001",
                                        "name" : "Solar System",
                                        "text" : "Mercury, Venus, Earth, Mars, Jupiter, Saturn, Neptune, (Pluto?)",
                                        "createdAt" : "-999999999-01-01T00:00:00",
                                        "isArchived" : true }]"""

  "POST /api/note/" should "return JSON-serialized note if storage returns note with provided name and text parameters" in {
    (mockStorage.post _).expects(fakeNote.name, fakeNote.text).returns(IO.pure(Right(fakeNote)))

    val (status, body) = run(Method.POST, "/note", Some(fakeSimpleNote.asJson))

    status shouldBe Status.Ok
    body shouldBe expectedJson
  }

  it should "return properly filled error model and HTTP code 400 if Json can not be parsed" in {
    val (status, body) = run(Method.POST, "/note", Some(testInvalidJson))

    status shouldBe Status.BadRequest
    body shouldBe ApiError.BadRequest.asJson
  }

  it should "return properly filled error model and HTTP code 422 if a note with such data already exists" in {
    (mockStorage.post _)
      .expects(fakeNote.name, fakeNote.text)
      .returns(IO.pure(Left(ApiError.LogicError(fakeSimpleNote.name))))

    val (status, body) = run(Method.POST, "/note", Some(fakeSimpleNote.asJson))

    status shouldBe Status.UnprocessableEntity
    body shouldBe ApiError.LogicError(fakeNote.name).asJson
  }

  it should "return properly filled error model and HTTP code 500 if unexpected error occurred" in {
    (mockStorage.post _).expects(fakeNote.name, fakeNote.text).returns(IO.raiseError(new RuntimeException))

    val (status, body) = run(Method.POST, "/note", Some(fakeSimpleNote.asJson))

    status shouldBe Status.InternalServerError
    body shouldBe ApiError.Unexpected.asJson
  }

  "GET /api/note/{uuid}" should "return JSON-serialized note if storage returns note with provided ID" in {
    (mockStorage.get _).expects(fakeNote.id).returns(IO.pure(Right(fakeNote)))

    val (status, body) = run(Method.GET, s"/note/${fakeNote.id}")

    status shouldBe Status.Ok
    body shouldBe expectedJson
  }

  it should "return properly filled error model and HTTP code 404 if ID is not found in the storage" in {
    (mockStorage.get _).expects(fakeNote.id).returns(IO.pure(Left(ApiError.NotFound(fakeNote.id))))

    val (status, body) = run(Method.GET, s"/note/${fakeNote.id}")

    status shouldBe Status.NotFound
    body shouldBe ApiError.NotFound(fakeNote.id).asJson
  }

  it should "return properly filled error model and HTTP code 500 if unexpected error occurred" in {
    (mockStorage.get _).expects(fakeNote.id).returns(IO.raiseError(new RuntimeException))

    val (status, body) = run(Method.GET, s"/note/${fakeNote.id}")

    status shouldBe Status.InternalServerError
    body shouldBe ApiError.Unexpected.asJson
  }

  "DELETE /api/note/{uuid}" should "return empty JSON-serialized note" in {
    (mockStorage.delete _).expects(fakeNote.id).returns(IO.pure(Right((): Unit)))

    val (status, body) = run(Method.DELETE, s"/note/${fakeNote.id}")

    status shouldBe Status.Ok
    body shouldBe emptyResponse
  }

  it should "return properly filled error model and HTTP code 500 if unexpected error occurred" in {
    (mockStorage.delete _).expects(fakeNote.id).returns(IO.raiseError(new RuntimeException))

    val (status, body) = run(Method.DELETE, s"/note/${fakeNote.id}")

    status shouldBe Status.InternalServerError
    body shouldBe ApiError.Unexpected.asJson
  }

  "PATCH /api/note/{uuid}" should "return JSON-serialized note if storage returns note" in {
    (mockStorage.patch _)
      .expects(fakeNote.id, fakeSimpleNoteOption.name, fakeSimpleNoteOption.text)
      .returns(IO.pure(Right(fakeNote)))

    val (status, body) = run(Method.PATCH, s"/note/${fakeNote.id}", Some(fakeSimpleNoteOption.asJson))

    status shouldBe Status.Ok
    body shouldBe expectedJson
  }

  it should "return JSON-serialized note if storage returns note with only name provided" in {
    val newFake = fakeSimpleNoteOption.copy(name = fakeSimpleNoteOption.name, text = None)

    (mockStorage.patch _).expects(fakeNote.id, fakeSimpleNoteOption.name, None).returns(IO.pure(Right(fakeNote)))

    val (status, body) = run(Method.PATCH, s"/note/${fakeNote.id}", Some(newFake.asJson))

    status shouldBe Status.Ok
    body shouldBe expectedJson
  }

  it should "return JSON-serialized note if storage returns note with only text provided" in {
    val newFake = fakeSimpleNoteOption.copy(name = None, text = fakeSimpleNoteOption.text)

    (mockStorage.patch _).expects(fakeNote.id, None, fakeSimpleNoteOption.text).returns(IO.pure(Right(fakeNote)))

    val (status, body) = run(Method.PATCH, s"/note/${fakeNote.id}", Some(newFake.asJson))

    status shouldBe Status.Ok
    body shouldBe expectedJson
  }

  it should "return JSON-serialized empty note and HTTP code 304 if none of the parameters are provided" in {
    def runEmpty(method: Method, path: String, body: Option[Json]): (Status, Boolean) = {
      val base = Request[IO](method, Uri.unsafeFromString(path))
      val request = body match {
        case Some(value) => base.withEntity(value)
        case None        => base
      }
      val response   = controller.routes.orNotFound.run(request).unsafeRunSync()
      val status     = response.status
      val bodyStatus = response.body.compile.toVector.unsafeRunSync().isEmpty
      (status, bodyStatus)
    }

    val newFake = fakeSimpleNoteOption.copy(name = None, text = None)

    (mockStorage.patch _).expects(fakeNote.id, None, None).returns(IO.pure(Left(ApiError(304, ""))))

    val (status, body) = runEmpty(Method.PATCH, s"/note/${fakeNote.id}", Some(newFake.asJson))

    status shouldBe Status.NotModified
    body shouldBe true
  }

  it should "return properly filled error model and HTTP code 400 if Json can not be parsed" in {
    val (status, body) = run(Method.PATCH, s"/note/${fakeNote.id}", None)

    status shouldBe Status.BadRequest
    body shouldBe ApiError.BadRequest.asJson
  }

  it should "return properly filled error model and HTTP code 404 if ID not found in storage" in {
    (mockStorage.patch _)
      .expects(fakeNote.id, fakeSimpleNoteOption.name, fakeSimpleNoteOption.text)
      .returns(IO.pure(Left(ApiError.NotFound(fakeNote.id))))

    val (status, body) = run(Method.PATCH, s"/note/${fakeNote.id}", Some(fakeSimpleNoteOption.asJson))

    status shouldBe Status.NotFound
    body shouldBe ApiError.NotFound(fakeNote.id).asJson
  }

  it should "return properly filled error model and HTTP code 500 if unexpected error occurred" in {
    (mockStorage.patch _)
      .expects(fakeNote.id, fakeSimpleNoteOption.name, fakeSimpleNoteOption.text)
      .returns(IO.raiseError(new RuntimeException))

    val (status, body) = run(Method.PATCH, s"/note/${fakeNote.id}", Some(fakeSimpleNoteOption.asJson))

    status shouldBe Status.InternalServerError
    body shouldBe ApiError.Unexpected.asJson
  }

  "POST /api/note/{uuid}?isArchived=<bool>" should "return JSON-serialized note if storage returns note with provided id and isArchived parameters" in {
    (mockStorage.archive _).expects(fakeNote.id, fakeNote.isArchived).returns(IO.pure(Right(fakeNote)))

    val queryMap       = Map("isArchived" -> fakeNote.isArchived.toString)
    val (status, body) = run(Method.POST, s"/note/${fakeNote.id}", None, queryMap)

    status shouldBe Status.Ok
    body shouldBe expectedJson
  }

  it should "return properly filled error model and HTTP code 404 if ID not found in storage" in {
    (mockStorage.archive _)
      .expects(fakeNote.id, fakeNote.isArchived)
      .returns(IO.pure(Left(ApiError.NotFound(fakeNote.id))))

    val queryMap       = Map("isArchived" -> fakeNote.isArchived.toString)
    val (status, body) = run(Method.POST, s"/note/${fakeNote.id}", None, queryMap)

    status shouldBe Status.NotFound
    body shouldBe ApiError.NotFound(fakeNote.id).asJson
  }

  it should "return properly filled error model and HTTP code 500 if unexpected error occurred" in {
    (mockStorage.archive _).expects(fakeNote.id, fakeNote.isArchived).returns(IO.raiseError(new RuntimeException))

    val queryMap       = Map("isArchived" -> fakeNote.isArchived.toString)
    val (status, body) = run(Method.POST, s"/note/${fakeNote.id}", None, queryMap)

    status shouldBe Status.InternalServerError
    body shouldBe ApiError.Unexpected.asJson
  }

  "GET /api/note/list?name=hello&text=world&orderBy=name" should "return JSON-serialized notes if all parameters are empty" in {
    (mockStorage.getAll _).expects(None, None, None, None, None).returns(IO.pure(Right(fakeNoteList)))

    val (status, body) = run(Method.GET, "/note/list")

    status shouldBe Status.Ok
    body shouldBe expectedListJson
  }

  it should "return JSON-serialized notes if some parameters are present" in {
    (mockStorage.getAll _)
      .expects(Some(fakeNote.name), Some(fakeNote.text), Some(fakeNote.isArchived), None, None)
      .returns(IO.pure(Right(fakeNoteList)))

    val queryParams =
      Map("name" -> fakeNote.name, "text" -> fakeNote.text, "isArchived" -> fakeNote.isArchived.toString)
    val (status, body) = run(Method.GET, "/note/list", None, queryParams)

    status shouldBe Status.Ok
    body shouldBe expectedListJson
  }

  it should "return properly filled error model and HTTP code 500 if unexpected error occurred" in {
    (mockStorage.getAll _).expects(None, None, None, None, None).returns(IO.raiseError(new RuntimeException))

    val (status, body) = run(Method.GET, "/note/list")

    status shouldBe Status.InternalServerError
    body shouldBe ApiError.Unexpected.asJson
  }
}
